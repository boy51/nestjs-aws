import { S3Client, S3ClientConfig } from '@aws-sdk/client-s3'
import { DynamicModule, Module } from '@nestjs/common'

@Module({})
export class S3Module {
  /** Do a thing yes great congrats */
  static register(config: S3ClientConfig): DynamicModule {
    return {
      module: S3Module,
      providers: [
        {
          provide: S3Client,
          useFactory: () => {
            return new S3Client(config)
          },
        },
      ],
      exports: [S3Client],
    }
  }
}
